#include <vector>
#include <system_error>
#include <iostream>
#include <TString.h>

#include "Core/CommonTools/interface/variables.h"
#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"

#include <protodarwin.h>

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::MissingET {

struct Filters {
    vector<TString> METnames;
    vector<int> METbitsToApply;

    // NOTE: this is taken from the python config file, check that the name
    //       still make sense.....
    Filters (int year)
    {
        switch (year) {
            case 2016:
                METnames = {"goodVertices", "globalSuperTightHalo2016Filter", "HBHENoiseFilter",
                            "HBHENoiseIsoFilter", "EcalDeadCellTriggerPrimitiveFilter",
                            "BadPFMuonFilter",  /*this one destroys Herwig16, for some reason*/
                            "BadPFMuonDzFilter", //always return null value, to be better studied
                            "BadChargedCandidateFilter", /*this one too, but anyway, it is no longer recommended for any */
                            "eeBadScFilter" /*this one is useless*/};
                
                METbitsToApply = {0, 1, 2, 3, 4, 5, /*6, 7,*/ 8}; // TODO
                break;
            case 2017:
            case 2018:
                // filters 6 and 9 always null, to be better investigated!!! filters 7 and 8 not recommended
                METnames = {"goodVertices", "globalSuperTightHalo2016Filter", "HBHENoiseFilter",
                            "HBHENoiseIsoFilter", "EcalDeadCellTriggerPrimitiveFilter",
                            "BadPFMuonFilter",  /*this one destroys Herwig16, for some reason*/
                            "BadPFMuonDzFilter",
                            "BadChargedCandidateFilter",
                            "eeBadScFilter",
                            "hfNoisyHitsFilter",
                            "ecalBadCalibFilter"};
                METbitsToApply = {0, 1, 2, 3, 4, 5, /*6, 7,*/ 8, /*9,*/ 10}; // TODO
                break;
            default:
                BOOST_THROW_EXCEPTION( invalid_argument(Form("%d is not (currently) not handled.", year)) );
        }

        cout << "MET bits:";
        for (auto bit: METbitsToApply)
            cout << ' ' << bit;
        cout << endl;
    }

    void operator() (MET * met, Event * event, vector<RecJet> * recJets)
    {
        // testing MET filters
        bool passFilters = true;
        // BadChargedCandidateFilter is no longer recommended
        for (size_t ibit = 0; ibit < METnames.size(); ++ibit) {

            bool bit = met->Bit.at(ibit);

            if (find(METbitsToApply.begin(), METbitsToApply.end(), ibit) != METbitsToApply.end())
                passFilters = passFilters && bit;
        }

        if (!passFilters)
            // reconstructed jets should be removed, so one may be tempted to remove the event...
            // but for MC, generated jets should *not* be removed -> just empty the list of rec jets
            recJets->clear();
    }

};

}
