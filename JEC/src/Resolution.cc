#include <random>
#include <limits>
#include <filesystem>

#include "Core/CommonTools/interface/toolbox.h"

#include "Core/JEC/interface/Resolution.h"

#include "Core/JEC/bin/matching.h"

#include <TFile.h>

using namespace std;
using namespace DAS;
using namespace JetEnergy;

namespace fs = filesystem;

static const auto eps = numeric_limits<double>::epsilon();

Resolution::Resolution (const fs::path& JERtables, int R, int seed, bool applySyst) : // TODO: add an option for PUPPI
    resolution   (JERtables.string() + '/' + JERtables.stem().string() + "_PtResolution_AK" + to_string(R < 6 ? 4 : 8) + "PFchs.txt"),
    resolution_sf(JERtables.string() + '/' + JERtables.stem().string() + "_SF_AK"           + to_string(R < 6 ? 4 : 8) + "PFchs.txt"),
    m_random_generator(seed), syst(applySyst)
{
    cout << __func__ << ' ' << JERtables << endl;

    fs::path tails = JERtables.string() + '/' + JERtables.stem().string() + "_PtResolution_FatTails_AK" + to_string(R < 6 ? 4 : 8) + "PFchs.root";
    if (fs::exists(tails)) {
        auto fTails = make_unique<TFile>(tails.c_str(), "READ");
        Matching<RecJet, GenJet>::h_kL = fTails->Get<TH3>("h_kL");
        Matching<RecJet, GenJet>::h_kR = fTails->Get<TH3>("h_kR");
        Matching<RecJet, GenJet>::h_kL->SetDirectory(0);
        Matching<RecJet, GenJet>::h_kR->SetDirectory(0);
    }
    else cout << "Tails will be ignored in the matching." << endl;
}

JME::JetParameters Resolution::getParams (float pt, float eta, float rho) const
{
    JME::JetParameters params = {{JME::Binning::JetPt , pt  },
                                 {JME::Binning::JetEta, eta },
                                 {JME::Binning::Rho   , rho }};
    return params;
}

JME::JetParameters Resolution::getParams (const RecJet& recjet, float rho) const
{
    auto p4 = recjet.p4;
    p4.Scale(recjet.JECs.front());
    auto recpt  = p4.Pt(),
         receta = p4.Eta();
    return getParams(recpt, receta, rho);
}

vector<float> Resolution::vJECs (const RecJet& recjet, const function<float(Variation)>& var) 
{
    auto scale = recjet.JECs.front();
    vector<float> vars = { scale * var(Variation::NOMINAL) };
    if (syst) {
        vars.push_back( scale * var(Variation::UP     ) );
        vars.push_back( scale * var(Variation::DOWN   ) );
    }
    return vars;
}

vector<float> Resolution::ScalingMethod (const RecJet& recjet, const GenJet& genjet, float rho)
{
    auto params = getParams(recjet, rho);

    auto var = [&](Variation v) {
        auto jer_sf = resolution_sf.getScaleFactor(params, v);
        auto recpt = recjet.CorrPt();
        auto dPt = recpt - genjet.p4.Pt();
        return max(0., 1. + (jer_sf - 1.) * dPt / recpt);
    };

    return vJECs(recjet, var);
}

vector<float> Resolution::StochasticMethod (const RecJet& recjet, float rho) 
{
    auto params = getParams(recjet, rho);
    float sigma = resolution.getResolution(params); // plain MC resolution from tables

    normal_distribution<float> d{0, sigma}; // TODO: make sure that this is the fastest solution
    auto delta = d(m_random_generator);
    auto var = [&](Variation v) {
        auto jer_sf = resolution_sf.getScaleFactor(params, v);
        return max(0., 1. + delta * sqrt(max(eps, jer_sf * jer_sf - 1.)));
    };

    return vJECs(recjet, var);
}
