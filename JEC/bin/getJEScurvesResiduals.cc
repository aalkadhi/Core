#include <cstdlib>
#include <string>
#include <iostream>
#include <filesystem>

#include <TFile.h>
#include <TROOT.h>
#include <TH2.h>
#include <TF1.h>
#include <TMath.h>

#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/variables.h"

#include "Core/JEC/interface/JESreader.h"

using namespace std;
using namespace DAS;

namespace fs = filesystem;

////////////////////////////////////////////////////////////////////////////////
/// Obtain JES corrections (L1 FastJet)
void getJEScurvesResiduals (TString input, TString output, float radius) 
{
    fs::path p = input.Data();
    cout << p << endl;
    assert(fs::exists(p) && fs::is_directory(p));
    cout << p.stem().c_str() << endl;

    TFile * f = TFile::Open(output, "RECREATE");

    cout << radius << endl;
    JESreader * jecs = new JESreader(input.Data(), p.stem().c_str(), radius);

    TH2 * h = new TH2D("residuals", "residuals", nPtBins, pt_edges.data(), nEtaBins, eta_edges.data());

    for (int i = 0; i <= h->GetNbinsX(); ++i)
    for (int j = 0; j <= h->GetNbinsY(); ++j) {
        double pt = h->GetXaxis()->GetBinCenter(i),
               eta = h->GetYaxis()->GetBinCenter(j);

        double corr = jecs->GetL2L3Residual(pt, eta);
        h->SetBinContent(i,j,corr);
    }
    h->Write();

    f->Close();
}

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    TH1::SetDefaultSumw2();
    gROOT->SetBatch();

    if (argc < 4) {
        cout << argv[0] << " input output radius\n"
             << "\twhere\tinput = JetMET tables\n"
             << "\t     \toutput = root files with histograms and fits\n"
             << "\t     \tradius = 0.4 or 0.8\n"
             << flush;
        return EXIT_SUCCESS;
    }

    TString input = argv[1],
            output = argv[2];

    float radius = atof(argv[3]);

    getJEScurvesResiduals(input, output, radius);
    return EXIT_SUCCESS;
}
#endif

