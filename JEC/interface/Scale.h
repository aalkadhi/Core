#ifndef DAS_JES_H
#define DAS_JES_H

#include <map>
#include <vector>
#include <string>
#include <filesystem>

#include <TString.h>

#include "CondFormats/JetMETObjects/interface/FactorizedJetCorrector.h"
#include "CondFormats/JetMETObjects/interface/JetCorrectorParameters.h"
#include "CondFormats/JetMETObjects/interface/JetCorrectionUncertainty.h"

#include "Core/Objects/interface/Jet.h"

namespace DAS::JetEnergy {

////////////////////////////////////////////////////////////////////////////////
/// Class to acces Jet Energy Scale files from JetMET and apply them.
class Scale  {

    bool //isMC, //!< flag, 1 = MC, 0 = data
         wFl;  //!< flag "with flavour" (i.e. w. L5 correction)

    // TODO: smart pointers? plain variables?
    FactorizedJetCorrector * L1Fast,         //!< level 1, for PU
                           * L2Relative,     //!< level 2, relative correction
                           * L3Absolute,     //!< level 3, absolute correction
                           * L2L3Residual;   //!< data residual corrections 
    std::map<int, FactorizedJetCorrector *> L5Flavor; //!< level 5, for flavour correction

    JetCorrectionUncertainty *uncProv; //!< provider for total uncertainty
public:
    std::vector<JetCorrectionUncertainty *> uncSources; //!< provider for single sources of uncertainty

    ////////////////////////////////////////////////////////////////////////////////
    /// Apply L1 correction on transverse momentum (to recover from PU effects)
    float GetL1Fast
        (float pt,  //!< transverse momentum
         float eta,  //!< pseudorapidity
         float area, //!< jet area
         float rho) //!< soft activity
        const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Apply L2 relative correction on transverse momentum
    float GetL2Relative
        (float pt,  //!< transverse momentum
         float eta) //!< pseudorapidity
        const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Apply L3  absolute correction on transverse momentum
    float GetL3Absolute
        (float pt,  //!< transverse momentum
         float eta) //!< pseudorapidity
        const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Apply L5 flavour corrections
    float GetL5Flavor
        (float pt, //!< transverse momentum
         float eta, //!< pseudorapidity
         int partonFlavour) //!< parton flavour (DIFFERENT from hadron flavour!)
             const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Apply L2L3 residual corrections to the transverse momentum and to the pseudorapidity
    ///
    /// *Note*: for data only!
    float GetL2L3Residual 
        (float pt,  //!< transverse momentum
         float eta) //!< pseudorapidity
        const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Get factor for nominal variations 
    float getJEC
        (const RecJet& jet, //!< reconstructed jet
         float rho //!< soft activity (from `PileUp` object)
         ) const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Get factor for nominal variations.
    float getJEC
        (float pt, //!< raw transverse momentum
         float eta, //!< raw pseudorapidity
         float area, //!< jet area
         float rho, //!< soft activity (from `PileUp` object)
         int pFl = 0 //!< parton flavour
         ) const;

    ////////////////////////////////////////////////////////////////////////////////
    /// get total uncertainty, up and down separately
    std::pair<float,float> getTotalUnc (float pt, float eta) const;

    ////////////////////////////////////////////////////////////////////////////////
    /// get total uncertainty up and down in a vector
    std::vector<float> getTotalUncVec (float pt, float eta) const;

    ////////////////////////////////////////////////////////////////////////////////
    /// get vector with all uncertainties
    /// (up and down for each source are stored in row)
    std::vector<float> getUncVec (float pt, float eta) const;

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    /// 
    /// TODO: add an argument to choose between PUPPI and CHS (currently CHS by def)
    Scale
        (std::filesystem::path dir, //!< directory containing the text files
         int R //!< radius (x10)
        );

    ////////////////////////////////////////////////////////////////////////////////
    /// Static array to return the names of all uncertainties.
    ///
    /// See https://twiki.cern.ch/twiki/bin/viewauth/CMS/JECUncertaintySources
    /// for more detailed information on their meaning
    static const std::vector<const char *> uncs;
};

}

#endif
