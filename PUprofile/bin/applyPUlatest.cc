#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <filesystem>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <boost/property_tree/info_parser.hpp>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/variables.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/ControlPlots.h"

#include "Core/PUprofile/interface/Latest.h"

#include <TString.h>
#include <TChain.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>

#include "Math/VectorUtil.h"

#include <protodarwin.h>

using namespace std;
using namespace DAS;

using namespace std;

namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::PUprofile {

////////////////////////////////////////////////////////////////////////////////
/// Reads `pileup_latest.txt` (provided centrally by CMS) to store the information
/// directly in the n-tuple.
void applyPUlatest 
        (const vector<fs::path>& inputs, //!< input ROOT files (n-tuple)
         const fs::path& output, //!< output ROOT file (n-tuple)
         const pt::ptree& config, //!< config handled with `Darwin::Tools::Options`
         const int steering, //!< parameters obtained from explicit options
         const DT::Slice slice = {1,0} //!< number and index of slice
        )
{
    cout << __func__ << ' ' << slice << " start" << endl;
    
    shared_ptr<TChain> tIn = DT::GetChain(inputs, "inclusive_jets");
    unique_ptr<TFile> fOut(DT_GetOutput(output));
    auto tOut = shared_ptr<TTree>(tIn->CloneTree(0));

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    auto isMC = metainfo.Get<bool>("flags", "isMC");
    if (isMC) BOOST_THROW_EXCEPTION( DE::BadInput("Only data may be used as input.",
                                      make_unique<TFile>(inputs.front().c_str() )) );

    auto PUlatest = config.get<fs::path>("corrections.PUprofile.latest");
    metainfo.Set<fs::path>("corrections", "PUprofile", "latest", PUlatest);
    PUprofile::Latest pu_functor(PUlatest);

    Event * event = nullptr;
    PileUp * pu = nullptr;
    tIn->SetBranchAddress("event", &event);
    tIn->SetBranchAddress("pileup", &pu);

    for (DT::Looper looper(tIn, slice); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;
        pu_functor(event, pu);
        if ((steering & DT::fill) == DT::fill) tOut->Fill();
    }

    metainfo.Set<bool>("git", "complete", true);
    fOut->cd();
    tOut->Write();

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of DAS::PUprofile namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Imports the PU information into the n-tuple\\"
                "/afs/cern.ch/cms/CAF/CMSCOMM/COMM_DQM/certification/Collisions1?/13TeV/PileUp/pileup_latest.txt",
                DT::config | DT::split | DT::fill);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<fs::path>("PUlatest", "corrections.PUprofile.latest", "json file with PU information (centrally provided)");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::PUprofile::applyPUlatest(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
