#include <cstdlib>
#include <iostream>

#include <TChain.h>
#include <TFile.h>
#include <TH1.h>

#include "Core/Objects/interface/Event.h"

#include <protodarwin.h>

using namespace std;

namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::Normalisation {

////////////////////////////////////////////////////////////////////////////////
/// Sum the weights from ROOT n-tuple. Mostly useful for MC to estimate the 
/// effective number of events, which is necessary to normalise.
void getSumWeights
        (const vector<fs::path>& inputs, //!< input ROOT files (n-tuple)
         const fs::path& output, //!< output ROOT file (histograms)
         const pt::ptree& config, //!< config handled with `Darwin::Tools::Options`
         const int steering, //!< parameters obtained from explicit options
         const DT::Slice slice = {1,0} //!< number and index of slice
        )
{
    cout << __func__ << ' ' << slice << " start" << endl;
    
    shared_ptr<TChain> tIn = DT::GetChain(inputs, "inclusive_jets");
    unique_ptr<TFile> fOut(DT_GetOutput(output));
    auto tOut = shared_ptr<TTree>(tIn->CloneTree(0)); // ghost tree: only to keep meta info

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);

    Event * evnt = nullptr;
    tIn->SetBranchAddress("event", &evnt);

    auto h = new TH1F("hSumWgt",";;#sum_{i=1}^{events} w_{i}", 1, -0.5, 0.5); // dummy thread-safe histogram

    for (DT::Looper looper(tIn, slice); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;
        auto w = evnt->genWgts.front();
        cout << w << endl;
        h->Fill(0.0, w);
    }
    h->Print("all");

    metainfo.Set<bool>("git", "complete", true);

    h->Write();
    tOut->Write();

    cout << __func__ << ' ' << slice << " end" << endl;
}

} // end of namespace DAS::Normalisation

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Get the sum of the weights.", DT::split);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file");
        
        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::Normalisation::getSumWeights(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
