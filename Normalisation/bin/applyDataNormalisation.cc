#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <map>
#include <filesystem>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/ControlPlots.h"

#include <TFile.h>
#include <TChain.h>
#include <TH2.h>

#include "applyDataNormalisation.h"

#include <protodarwin.h>

using namespace std;

namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::Normalisation {

////////////////////////////////////////////////////////////////////////////////
/// Apply the the kinematic selection, ensuring that events in the dataset have fired a trigger and
/// correcting the weight of the events with the associated trigger prescales, trigger efficiency,
/// and total luminosity.
/// The weight $\frac{ \text{Prescale} * \mathcal{L}^{-1} }{ \epsilon_\text{trigger} } $ is applied by event.
/// The argument 'strategy' defines the selection criterion of the events and the calculation of the weight
/// (see functor in applyDataNormalisation.h).
void applyDataNormalisation
        (const vector<fs::path>& inputs, //!< input ROOT files (n-tuple)
         const fs::path& output, //!< output ROOT file (n-tuple)
         const pt::ptree& config, //!< config handled with `Darwin::Tools::Options`
         const int steering, //!< parameters obtained from explicit options
         const DT::Slice slice = {1,0} //!< number and index of slice
        )
{
    cout << __func__ << ' ' << slice << " start" << endl;
    
    shared_ptr<TChain> tIn = DT::GetChain(inputs, "inclusive_jets");
    unique_ptr<TFile> fOut(DT_GetOutput(output));
    auto tOut = shared_ptr<TTree>(tIn->CloneTree(0));

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    auto isMC = metainfo.Get<bool>("flags", "isMC");
    if (isMC)
        BOOST_THROW_EXCEPTION( DE::BadInput("Only real data may be used as input.", metainfo) );

    Event * event = nullptr;
    Trigger * trigger = nullptr;
    tIn->SetBranchAddress("event", &event);
    tIn->SetBranchAddress("trigger", &trigger);
    vector<FourVector> * hltJets = nullptr;
    vector<RecJet> * recJets = nullptr;
    tIn->SetBranchAddress("hltJets", &hltJets);
    tIn->SetBranchAddress("recJets", &recJets);

    int year = metainfo.Get<int>("flags", "year");

    // defining functor to apply data prescales
    auto lumi_file    = config.get<fs::path>("corrections.normalisation.luminosities"),
         turnon_file  = config.get<fs::path>("corrections.normalisation.turnons"     ),
         efficiencies = config.get<fs::path>("corrections.normalisation.efficiencies");
    auto strategy = config.get<string>("corrections.normalisation.strategy"),
         method   = config.get<string>("corrections.normalisation.method"  );
    Normalisation::Functor normalisation(lumi_file, turnon_file, efficiencies, strategy, method, year);
    fOut->cd();

    // a few control plots
    ControlPlots::isMC = false;
    ControlPlots corrNoTrigEff("corrNoTrigEff");

    //vector<ControlPlots> corr; // TODO
    //for (int i = 0; i < metainfo.GetNRecEvWgts(); i++)
    //    corr.push_back(ControlPlots(metainfo.GetRecEvWgt(i)));

    for (DT::Looper looper(tIn, slice); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        if (recJets->size() == 0) continue;

        RecJet leadingJet = normalisation(*event, *recJets, *hltJets, *trigger);
        // the normalisation itself is done in normalisation
        // TODO: revisit the logic (having inputs modified + a return object may be confusing)

        // TODO: reimplement the logic to avoid a selection on the weight
        if (event->recWgts.front() <= 0) continue;

        if ((steering & DT::fill) == DT::fill) tOut->Fill();

        float evtWgts = event->recWgts.front();
        float efficiency = normalisation.eff(leadingJet);

        if (efficiency <= 0) continue;

        corrNoTrigEff(*recJets, evtWgts*efficiency); // compensate
        //for (size_t i = 0; i < corr.size(); ++i)
        //    corr.at(i)(*recJets, event->recWgts.at(i)); // TODO

        // Exclusive curves are filled (i.e. one event can populate only a trigger curve).
        // The ibit value is determined by
        // the leading jet but also the other jets of the event can populate the trigger curve.
        for (auto& jet: *recJets) {
            float y = jet.AbsRap();
            float w = event->recWgts.front();
            normalisation.eff.contribs.at(normalisation.ibit)->Fill(jet.CorrPt(), y, w);
        }
    }

    corrNoTrigEff.Write(fOut.get());
    //for (size_t i = 0; i < corr.size(); ++i)
    //    corr.at(i).Write(fOut.get()); // TODO
    TDirectory * controlplots = fOut->mkdir("controlplots");
    controlplots->cd();
    for (TH2 * h: normalisation.eff.contribs) {
        h->SetDirectory(controlplots);
        h->Write();
    }

    metainfo.Set<bool>("git", "complete", true);
    fOut->cd();
    tOut->Write();

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of DAS::Normalisation namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Normalise the data samples to the luminosity. Prescaled events may either "
                            "be normalised to the respective trigger luminosities or using trigger precales.\n"
                            "Existing strategies:\n"
                            " - 'pt': assume that the jets are sorted in pt and try to match the leading jet"
                            " - 'eta': sort the jets in eta and try to match the most fwd and second most fwd",
                            DT::config | DT::split | DT::fill);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<fs::path>("luminosities", "corrections.normalisation.luminosities", "2-column file with luminosity per trigger")
               .arg<fs::path>("turnons"     , "corrections.normalisation.turnons"     , "2-column file with turn-on per trigger"   )
               .arg<fs::path>("efficiencies", "corrections.normalisation.efficiencies", "output ROOT file of `getTriggerTurnons`"  )
               .arg<string>("strategy", "corrections.normalisation.strategy", "'eta' or 'pt'"        )
               .arg<string>("method"  , "corrections.normalisation.method"  , "'prescales' or 'lumi'");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::Normalisation::applyDataNormalisation(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
