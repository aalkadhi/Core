#ifndef _DAS_CLASSES_
#define _DAS_CLASSES_

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Lepton.h"
#include "Core/Objects/interface/Photon.h"
#include "Core/Objects/interface/Event.h"

DAS::GenJet genjet;
DAS::RecJet recjet;

DAS::GenMuon genmuon;
DAS::RecMuon recmuon;

DAS::GenPhoton genphoton;
DAS::RecPhoton recphoton;

DAS::Event event;
DAS::Trigger dastrigger;
DAS::PileUp pileup;
DAS::MET met;
DAS::PrimaryVertex primaryvertex;
DAS::SecondaryVertex secondaryvertex;

#endif
