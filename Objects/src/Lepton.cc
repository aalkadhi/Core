#include "Core/Objects/interface/Lepton.h"

using namespace DAS;

GenMuon::GenMuon () :
    Q(0), weights(1,1)
{}

RecMuon::RecMuon () :
    GenMuon(), selectors(0), scales(1,1)
{}

float RecMuon::RawPt () const { return p4.Pt(); }
float RecMuon::CorrPt (size_t i) const { return scales.at(i)*p4.Pt(); }

// TODO: float RecMuon::CorrE (size_t i) const; // preserve mass
