#ifndef _Jet_
#define _Jet_

#include <vector>

#include "Core/CommonTools/interface/variables.h"

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// class GenJet
///
/// Contains four-momentum and HF composition.
struct GenJet {

    FourVector p4; //!< Four-momentum
    int nBHadrons, //!< Number of *B* hadrons (0, 1 or 2+)
        nCHadrons; //!< Number of *C* hadrons (0, 1+ in 2017; 0, 1, 2+ in 2016)
    int partonFlavour; //!< Parton flavour (PDG ID)

    std::vector<float> weights; //!< jet weight (expected to be trivial for all-flavour inclusive jet cross section)
    
    ///
    /// Constructor (trivial)
    GenJet ();

    float Rapidity () const; //!< return rapidity 
    float AbsRap () const; //!< return absolute rapidity 
};

////////////////////////////////////////////////////////////////////////////////
/// class RecJet
///
/// Contains:
///  - four-momentum, 
///  - HF discriminants,
///  - information for the application of the JES corrections
///  - the JEC factors after applicaton of the JES correctoon and JER smearing (additional entries to the vector include the systematic effects)
///  - the HF content (only relevant for simulation; trivial for data)
/// 
struct RecJet : public GenJet {

    // for jet energy scale corrections
    float area; //!< Jet area (should be peaked at pi*R^2), used for the JES corrections

    ////////////////////////////////////////////////////////////////////////////////
    /// class deepjet
    ///
    /// Contains the seven values describing the probability for the jet to be heavy-flavoured
    struct Tagger { // TODO: make optional?
        float probc,    //!< Probability for the jet to contain exactly one *C* (in 2016) or at least one *C* *in 2017)
              //probcc,   //!< Probability for the jet to contain at least to two *C* (trivial value in 2017)
              probb,    //!< Probability for the jet to contain exactly one *B* hadron 
              probbb,   //!< Probability for the jet to contains at least two *B* hadrons
              problepb, //!< Probability for the jet to contains at least two *B* hadrons
              probuds,  //!< Probability for the jet to be a quark jet with no HF hadron
              probg;    //!< Probability for the jet to be a gluon jet with no HF hadron

        inline float B    () const { return probb+probbb+problepb; }
        inline float CvsL () const { return probc/(probc+probuds+probg); }
        inline float CvsB () const { return probc/(probc+probb+probbb+problepb); }
    } DeepJet;

    std::vector<float> JECs;    //!< correction factors for pt after JECs, together with the different systematics.

    ///
    /// Constructor (trivial)
    RecJet ();

    float RawPt () const; //!< w/o JES correction
    float CorrPt          //!< w. JES correction
        (size_t i = 0) const; //!< index in JECs vector
    float RawEta () const; //!< w/o JES correction
    float CorrEta          //!< w. JES correction
        (size_t i = 0) const; //!< index in JECs vector

    //FourVector CorrP4
    //    (size_t i = 0) const; // TODO
};

}


#if defined(__ROOTCLING__)
#pragma link C++ class DAS::GenJet +;
#pragma link C++ class std::vector<DAS::GenJet> +;

#pragma link C++ class DAS::RecJet +;
#pragma link C++ class DAS::RecJet::Tagger +;
#pragma link C++ class std::vector<DAS::RecJet> +;

#pragma link C++ class std::vector<FourVector > +;
#endif

#endif
