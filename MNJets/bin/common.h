#ifndef DAS_MN_OBS
#define DAS_MN_OBS
#include <list>
#include <vector>
#include "Core/Objects/interface/Jet.h"

////////////////////////////////////////////////////////////////////////////////
/// Contains the classes that are needed by getMNobservables.cc
/// to build the histograms.
/// Two types of objects are present here: *Obs2Jets* and *ObsMiniJets* that 
/// contains structures to store the observables themselves (they are higher
/// level physics objects) and *Hist* that contains histograms.
namespace DAS::MN {

////////////////////////////////////////////////////////////////////////////////
/// Implements MN jets observables depending on the 2 Mueller-Navelet jets of the event.
/// Definition of these observables:
/// - $p_{T,ave} = \frac{p_{T,FWD}+p_{T,BWD}}{2}$
/// - $p_{T,FWD}$ transverse momentum of the forward jet
/// - $p_{T,BWD}$ transverse momentum of the backward jet
/// - $p_{T,RatMN} = min \left( \frac{P_{T,FWD}{P{T,BWD}} , \frac{P_{T,BWD}{P{T,FWD}} \right)$
/// - $\pi - \Dela \phi$ is the azimuthal distance between the two Mueller-Navelet Jets
///   (mind that the function Obs2Jets::DPhi() returns in fact $\pi - \Dela \phi$)
/// - $\cos\left( n \left( \pi - \Delta \phi \right) \right)$ with $n = 1,2,3,4,...$
///   (implemented in CosDPhi() which directly returns this observable)
struct Obs2Jets {

    Obs2Jets ( const DAS::RecJet & Fwd, const DAS::RecJet & Bwd );
    Obs2Jets ( const DAS::GenJet & Fwd, const DAS::GenJet & Bwd );

    float PtAve () const;
    float PtFWD () const;
    float PtBWD () const;
    float PtRatMN () const;
    float DEta () const;
    float DPhi () const;
    float CosDPhi (int n //!< order
            ) const;
    size_t size () const;

    const float weightFWD, //!< weight of the most forward jet
                weightBWD; //!< weight of the most backward jet
    const float weight;    //!< product of weightFWD and weightBWD

private:
    std::list<DAS::FourVector> LeadingJets; //!< The most forward and the most backward jets.
                                            //!< The size is always 2 and and the type is taken
                                            //!< in consistency with ObsMiniJets.
};

////////////////////////////////////////////////////////////////////////////////
/// Implements observables based on the jets that are emmited in-between the
/// Mueller-Navelet jets. These jets are called "mini jets".
///
/// Definition of these observavbles ($N$ is the number of jets in the event,
/// and the jets are labeled $i \in (0,N-1)$, including the two Mueller-Navelet jets,
//  the jets are assumed ordred in rapidity):
/// - PtAveMini --> $ \left\langle P_{T,Mini} \right\rangle = \frac{1}{N-2} \sum_{i=1}^{N-2} p_{T,i} $
/// - DEtaAveMini --> &  \left\langle \Delta \eta_{Mini} \right\rangle =  \frac{1}{N-3} \sum_{i=1}^{N-3} \left\vert \eta_{i} - \eta_{i+1} \right\vert$
/// - REtaAveMini --> $ R_{k} = \frac{1}{N-3} \sum_{i=1}^{N-3} \frac{\eta_{i}}{\eta_{i+1}} $
///   // TODO Check this and compare with the standard definition.
/// - RPtExpDEta --> $ R_{ky} = \frac{1}{N-3} \sum_{i=1}^{N-3} min \left( \frac{P_{T,i}}{P_{T,i+1}}, \frac{P_{T,i+1}}{P_{T,i}} \right) \exp \left( \eta_{i} - \eta_{i+1} \right) $
struct ObsMiniJets {

    ObsMiniJets ( const std::vector<DAS::RecJet> & recJets );
    ObsMiniJets ( const std::vector<DAS::GenJet> & genJets );

    float PtAveMini () const;
    float DEtaAveMini () const;
    float REtaAveMini () const;
    float RPtExpDEta () const;
    size_t size () const;

    const float weight;

private:
    std::list<DAS::FourVector> MiniJets; //!< The jets that are between the Mueller-navelet Jets.
                                         //!< The size should be >1.
};

} // end of namespace MN_Helper
#endif
