#ifndef DAS_CONTROL_PLOT
#define DAS_CONTROL_PLOT

#include <vector>
#include <optional>

#include <TH1.h>
#include <TH2.h>
#include <TDirectory.h>
#include <TString.h>

#include "Core/Objects/interface/Jet.h"

namespace DAS {

struct ControlPlots {

    static bool isMC;

    const TString name;

    std::optional<TH1F> genpt, recpt;
    std::optional<TH2F> genpt_y, recpt_y;
    std::optional<TH2F> genMjj_y, recMjj_y;
    std::optional<TH2F> genpt0_N, recpt0_N;
    std::optional<TH2F> genpt_n, recpt_n;
    // TH3 TODO: pt eta rho plots
    // TH2 pt_N
    // TH2 ptmax_Dphi

    ControlPlots (TString Name);

    void operator()
        (const std::vector<RecJet>& recjets,
         const float& evW,
         size_t iJEC = 0,
         size_t iWgt = 0);

    void operator()
        (const std::vector<GenJet>& genjets,
         const float& evW,
         size_t iWgt = 0);

    void Write (TDirectory * D);
};

}
#endif
