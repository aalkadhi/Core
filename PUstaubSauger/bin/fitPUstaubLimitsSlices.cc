#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <map>
#include <limits>
#include <filesystem>

#include "Core/CommonTools/interface/variables.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>

#include <TString.h>
#include <TFile.h>
#include <TH2.h>
#include <TRegexp.h>
#include <TF1.h>

#include <protodarwin.h>

using namespace std;

namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

static const auto feps = numeric_limits<float>::epsilon();

namespace DAS::PUstaub {

////////////////////////////////////////////////////////////////////////////////
/// Dummy class/wrapper for `getPUstaubLimitsSlices`,
/// just used to split the code into smaller subroutines.
struct SliceSauger {

    vector<TH2 *> h2s, //!< `recptLogWgt` per slice
                  n2s, //!< `recptLogWgt` per slice, normalised to respective weights
                  s2s; //!< squared uncertainty from `recptLogWgt` per slice, normalised to respective weights
    int nslices; //!< number of slices (determined from `inputs`)
    map<int, map<int, float>> contents, //!< number of entries per pt bin and per slice
                              norm_contents, //!< weighted / effective number of entries per pt bin and per slice
                              errors; //!< cumulative relative uncertainty in % per pt bin and per slice
    map<int, map<int, bool>> Bads, //!< flag for bad contributions per pt bin and per slice
                             Mins; //!< flag for improving cumulative relative statistical uncertainty
    map<int, float> removed_events; //!< contains the fraction of removed events per slice in %

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    ///
    /// Using histograms from clean directory, where only the hard scale cut was applied.
    /// So these histograms were not corrected according to the <PUcleaning> argument
    /// (cut-off file) of `getPUstaub` command.
    ///
    /// Extracts the histograms from the outputs
    SliceSauger (vector<fs::path> slices) :
        nslices(slices.size())
    {
        assert(nslices > 0);
        for (auto slice: slices) {

            TString fname = slice.filename().c_str(),
                    title = "",
                    name = "";
            TFile * f = TFile::Open(fname);

            // Deduce slice edges
            if (fname.Contains("Inf")) title = fname(TRegexp("[0-9]*toInf")); // e.g. 3200toInf
            else                       title = fname(TRegexp("[0-9]*to[0-9]*")); // e.g. 1000to1400
            name = title(TRegexp("[0-9]*"));

            TH2 * h2 = dynamic_cast<TH2 *>(f->Get("clean/recptLogWgt")); // Only hard scale cut was applied here
            h2->SetNameTitle("h" + name, title);
            h2->SetDirectory(0);

            TH2 * n2 = dynamic_cast<TH2 *>(h2->Clone("n" + name));
            n2->SetTitle(title);
            n2->Reset();
            n2->SetDirectory(0);

            TH2 * s2 = dynamic_cast<TH2 *>(h2->Clone("s" + name));
            s2->SetTitle(title);
            s2->Reset();
            s2->SetDirectory(0);

            for (int i = 1; i <= h2->GetNbinsY(); ++i) {
                float logw = h2->GetYaxis()->GetBinCenter(i);
                float w = exp(logw); // w = (rec event wgt) * (gen event wgt)

                for (int j = 1; j <= h2->GetNbinsX(); ++j) {
                    float content = h2->GetBinContent(j,i); // Number of leading jets in this recpt bin
                    content *= w;
                    n2->SetBinContent(j,i,content);
                    content *= w; // sigma = w * sqrt(binCont) => sigma^2 = w^2 * binCont
                    s2->SetBinContent(j,i,content);
                }
            }

            h2s.push_back(h2);
            n2s.push_back(n2);
            s2s.push_back(s2);
            f->Close();
        }

        auto mySort = [](const char * c) {
            return [c](TH2 * a, TH2 * b) {
                TString A = a->GetName(), B = b->GetName();
                A.ReplaceAll(c, "");
                B.ReplaceAll(c, "");
                return A.Atoi() < B.Atoi();
            };
        };
        sort(h2s.begin(), h2s.end(), mySort("h"));
        sort(n2s.begin(), n2s.end(), mySort("n"));
        sort(s2s.begin(), s2s.end(), mySort("s"));
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Create matrix and normalise by row
    ///
    /// The following structure members are initialized:
    /// map<int, map<int, float>> contents, //!< number of entries per pt bin and per slice
    ///                           norm_contents, //!< weighted / effective number of entries per pt bin and per slice
    ///                           errors; //!< relative uncertainty in % per pt bin and per slice
    ///
    /// which will be later used in the following functions
    void FillContentMap (shared_ptr<TFile> f)
    {
        for (int ipt = 1; ipt <= nPtBins; ++ipt) {
            int lowedge = h2s.front()->GetXaxis()->GetBinLowEdge(ipt);
            gFile->cd();
            TDirectory * d = gFile->mkdir(Form("pt%d", lowedge));

            vector<TH1 *> h1s, n1s, s1s;

            // nominal value
            TDirectory * dd = d->mkdir("contents");
            dd->cd();
            for (TH2 * h2: h2s) {
                TString name = Form("slice_%s", h2->GetName());
                TH1 * h1 = h2->ProjectionY(name,ipt,ipt);
                h1s.push_back(h1);
            }
            for (int isl = 1; isl <= nslices; ++isl) {
                TH1 * h1 = h1s.at(isl-1); // First element of a vector is .at(0)
                if (f != nullptr) h1->Write();
                contents[ipt][isl] = h1->Integral();
            }
            d->cd();

            // normalised nominal value
            dd = d->mkdir("norm_contents");
            dd->cd();
            for (TH2 * n2: n2s) {
                TString name = Form("slice_%s", n2->GetName());
                TH1 * n1 = n2->ProjectionY(name,ipt,ipt);
                n1s.push_back(n1);
            }
            for (int isl = 1; isl <= nslices; ++isl) {
                TH1 * n1 = n1s.at(isl-1); // First element of a vector is .at(0)
                if (f != nullptr) n1->Write();
                norm_contents[ipt][isl] = n1->Integral();
            }
            d->cd();

            // uncertainty
            dd = d->mkdir("cumul_stat_unc");
            dd->cd();
            for (TH2 * s2: s2s) {
                TString name = Form("slice_%s", s2->GetName());
                TH1 * s1 = s2->ProjectionY(name,ipt,ipt);
                s1s.push_back(s1);
            }
            for (int isl = 1; isl <= nslices; ++isl) {
                TH1 * s1 = s1s.at(isl-1); // First element of a vector is .at(0)
                if (f != nullptr) s1->Write();
                errors[ipt][isl] = s1->Integral();
            }
        }

        // one more step to determine the cumulative relative uncertainty
        auto cumul_cont = norm_contents;
        for (int ipt = 1; ipt <= nPtBins; ++ipt) {

            // make the cumulative sums
            for (int isl = nslices; isl >= 1; --isl) { // start from above slices
                // Trick! Defining a new map element is auto-set to zero.
                // So if `isl = nslices` the element `errors[ipt][isl+1] = 0`
                cumul_cont[ipt][isl] = cumul_cont[ipt][isl] + cumul_cont[ipt][isl+1];
                errors[ipt][isl] = errors[ipt][isl] + errors[ipt][isl+1];
            }

            // normalise errors
            for (int isl = 1; isl <= nslices; ++isl) {
                if (cumul_cont[ipt][isl] > 0) {
                    float error = errors[ipt][isl] / pow(cumul_cont[ipt][isl],2);
                    errors[ipt][isl] = 100 * sqrt(error);
                }
            }
            // Just removing the extra empty element that was created
            assert(cumul_cont[ipt][nslices+1] < feps && errors[ipt][nslices+1] < feps);
            cumul_cont[ipt].erase(prev(cumul_cont[ipt].end()));
            errors[ipt].erase(prev(errors[ipt].end()));
            assert(cumul_cont[ipt].size() == errors[ipt].size());
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Find "bad" bins, i.e. bins that should be excluded.
    ///
    /// The Bads structure member is filled:
    /// map<int, map<int, bool>> Bads, //!< flag for bad contributions per pt bin and per slice
    ///
    /// 2 conditions:
    /// - at 30 entries for a low pthat slice,
    /// - the absolute contribution (effective number of entries) of a low pthat
    ///   slice should never be larger than half of the absolute contribution
    ///   of any higher pthat slice.
    void FillFlagBadMap ()
    {
        for (int ipt = 1; ipt <= nPtBins; ++ipt) {
            bool AlwaysGood = false;
            for (int isl = 1; isl < nslices; ++isl) {

                if (AlwaysGood) { // As soon as a good slice is found, next slices are also assumed good
                    Bads[ipt][isl] = false; // slice is good
                    continue;
                }

                float content = contents.at(ipt).at(isl);

                // True means slice is bad, False means slice is good
                bool Bad = content < 50 || !Mins[ipt][isl]; // i.e. the contribution from the slice should decrease
                Bads[ipt][isl] = Bad;
                AlwaysGood = !Bad;
            }
            Bads[ipt][nslices] = false; // the last slice is always entirely taken
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Deduce if including a slice for a given pt bin reduces the rel. cumulative
    /// stat. unc. (scenaio that we want) of that pt bin.
    ///
    /// Two functions exist currently, both give very similar results
    /// - FillFlagMinMapByMinSlice (): Comparing to the slice which holds the min cumul. rel. unc.
    /// - FillFlagMinMapByCompTwoSlices (): Comparing two the cumul. rel. unc of two consecutive slices
    ///
    /// The Mins structure member is filled:
    /// map<int, map<int, bool>> Mins; //!< flag for improving cumulative relative statistical uncertainty
    void FillFlagMinMapByMinSlice ()
    {
        for (int ipt = 1; ipt <= nPtBins; ++ipt) {
            auto& error = errors.at(ipt);
            auto MinEl = min_element(error.begin(), error.end(),
                     [](const auto& l, const auto& r)
                     { return l.second > feps && l.second <= r.second; });
            // Warning! 2.5 and 5.0 are choosen arbitarily
            for (int isl = 1; isl <= nslices; ++isl)
                Mins[ipt][isl] = (isl <= 3) ? (isl >= MinEl->first) || (isl < MinEl->first && errors[ipt][isl] < 2.5 * MinEl->second)
                                            : (isl >= MinEl->first) || (isl < MinEl->first && errors[ipt][isl] < 5.0 * MinEl->second);
        }
    }

    void FillFlagMinMapByCompTwoSlices ()
    {
        for (int ipt = 1; ipt <= nPtBins; ++ipt)
        for (int isl = 1; isl <= nslices; ++isl) {
            float err1 = errors[ipt][isl],
                  err2 = errors[ipt][isl+1];

            // - True (good slice) means that including this slice reduces the rel. unc.
            //   or increases (worsen) by only a small number
            // - False (bad slice) means that including this slice increases the rel. uns.
            // Warning! 0.5% is choosen arbitarily
            Mins[ipt][isl] = err1 - err2 < 0.5;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Avoid to remove certain bins:
    /// - one random bin in the middle of the slice
    /// - or one random bin in the tail
    void RegulariseBadMap ()
    {
        // regularising pt bins for each slice separetely
        for (int isl = 1; isl < nslices; ++isl)  {

            bool AlwaysGood = false;
            for (int ipt = nPtBins-1; ipt >= 2; --ipt) {
                if (AlwaysGood) {
                    Bads[ipt][isl] = false;
                    continue;
                }
                if (contents.at(ipt).at(isl) < 30) {
                    Bads[ipt][isl] = true;
                    continue;
                }
                bool& bad1 = Bads[ipt-1][isl],
                    & bad2 = Bads[ipt  ][isl],
                    & bad3 = Bads[ipt+1][isl];

                AlwaysGood = !((!bad1) && (!bad2) && (!bad3));

                if ((!bad1) && (!bad3)) bad2 = false;
                if (  bad1  &&   bad3 ) bad2 = true;
            }
        }
        // the last slice is always entirely taken
        for (int ipt = 1; ipt <= nPtBins; ++ipt) Bads[ipt][nslices] = false;
    }


    ////////////////////////////////////////////////////////////////////////////////
    /// Estimates the fraction of events removed per pthat slice
    ///
    /// The removed_events structure member is filled:
    /// map<int, float> removed_events; //!< contains the fraction of removed events per slice in %
    void GetFractionOfRemovedEvents ()
    {
        for (int isl = 1; isl <= nslices; ++isl) {
            float numerator = 0, denominator = 0;
            for (int ipt = 1; ipt <= nPtBins; ++ipt) {
                float content = contents.at(ipt).at(isl);
                denominator += content; // Total sum of entries per slice
                if (Bads.at(ipt).at(isl)) numerator += content; // Total sum of removed entries per slice
            }
            if (denominator > 0)
                removed_events[isl] = 100. * numerator / denominator;
            else
                removed_events[isl] = 0.;
        }
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// Print with beautiful colours on terminal
    ///
    /// The first column gives the low edge of the current pt bin.
    /// The header gives the pthat slice.
    ///
    /// For each slice & pt bin:
    /// - absolute number of jets in that bin,
    /// - relative cumulative statistical uncertainty
    ///   (cumulative from the right hand side!).
    ///
    /// Colour code:
    ///  - excluded contributions are shown in red;
    ///  - statistically most precise contribution is shown in green;
    ///  - additional contributions shown in normal colour.
    void Print (bool verbose)
    {
        static auto& cout = verbose ? ::cout : DT::dev_null;

        cout << "ptmin   " << left;
        for (auto h2: h2s) cout << setw(18) << h2->GetTitle();
        cout << endl;

        cout << right;
        
        for (int ipt = 1; ipt <= nPtBins; ++ipt) {
            int ptmin = h2s.front()->GetXaxis()->GetBinLowEdge(ipt);

            // content
            cout << "\x1B[30m\e[0m" << setw(4) << ptmin;
            for (int isl = 1; isl <= nslices; ++isl)  {

                int content = contents.at(ipt).at(isl);
                float error = errors.at(ipt).at(isl);
                bool Bad = Bads.at(ipt).at(isl);

                if (Bad) cout << red;
                else     cout << green;

                cout << setw(11) << content 
                     << setw( 6) << setprecision(2) << fixed << error << '%';
            }
            cout << '\n';
        }
        if (removed_events.size() > 0) {
            cout << normal << "    " << setprecision(6);
            for (int isl = 1; isl <= nslices; ++isl)
                cout << setw(17) << removed_events.at(isl) << '%';
        }
        cout << def << endl;
    }

    //////////////////////////////////////////////////////////////////////////////// 
    /// Write cuts on weight per pt bin
    pt::ptree Write ()
    {
        pt::ptree table;
        for (int ipt = 1; ipt <= nPtBins; ++ipt) {
            int ptmin = n2s.front()->GetXaxis()->GetBinLowEdge(ipt);
            for (int isl = 1; isl <= nslices; ++isl) {
                if (Bads.at(ipt).at(isl)) continue;

                TH2 * n2 = n2s.at(isl-1); // First element of a vector is .at(0)
                for (int iwgt = n2->GetNbinsY(); iwgt > 0; --iwgt) {
                    float content = n2->GetBinContent(ipt, iwgt);
                    if (content < feps) continue;
                    float maxlogw = n2->GetYaxis()->GetBinLowEdge(iwgt+1);
                    table.put<float>(to_string(ptmin), maxlogw);
                    break;
                }
                break;
            }
        }
        return table;
    }
};

////////////////////////////////////////////////////////////////////////////////
/// Exclude contributions from PU at high rec pt coming from low pt hat slices
/// by comparing directly the weighted contribution from each slice in each rec pt bin.
/// In addition, a check is performed by looking at the cumulative statistical uncertainty
/// from the highest pt hat slice to the current pt hat slice (i.e. we check how adding a 
/// lower pt hat slice improves or degrades the statistics of a given rec pt bin).
/// This logic was set up for the SMP-20-011 analysis, and should be revisited
/// for other samples.
///
/// Output is similar to that of `fitPUstaubLimitsFlat` with a max weight per rec pt bin.
void fitPUstaubLimitsSlices
        (vector<fs::path> inputs, //!< histograms, one file per slice
         const fs::path& outputRoot, //!< control plots
         const fs::path& outputTxt, //!< two-column cut-off file
         const pt::ptree& config, //!< config handled with `Darwin::Tools::Options`
         const int steering //!< parameters obtained from explicit options
        )
{
    cout << __func__ << " start" << endl;

    inputs = DT::GetROOTfiles(inputs);
    PUstaub::SliceSauger ss(inputs);

    auto f = make_shared<TFile>(outputRoot.c_str(), "RECREATE");
    ss.FillContentMap(f);
    ss.FillFlagMinMapByMinSlice();
    ss.FillFlagBadMap();
    ss.GetFractionOfRemovedEvents();
    bool verbose = (steering & DT::verbose) == DT::verbose;
    ss.Print(verbose);

    pt::ptree table = ss.Write();
    pt::write_info(outputTxt.string(), table);

    cout << __func__ << " stop" << endl;
}

} // end of DAS::PUstaub namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        vector<fs::path> inputs;
        fs::path outputRoot, outputTxt;

        DT::Options options("Determines the maximum weight in jet high-pileup samples.\n"
                            "NB: this executable expect a sample in slices!");
        options.inputs("inputs", &inputs, "input ROOT files, with exactly one file per slice")
               .output("outputRoot", &outputRoot, "output ROOT file with histograms")
               .output("outputTxt" , &outputTxt , "output 2-column file with limits");

        const auto& config = options(argc, argv);
        const int steering = options.steering();

        DAS::PUstaub::fitPUstaubLimitsSlices(inputs, outputRoot, outputTxt, config, steering);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
