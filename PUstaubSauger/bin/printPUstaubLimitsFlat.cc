#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <filesystem>

#include <TString.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>

#include "Math/VectorUtil.h"

#include <protodarwin.h>

using namespace std;

namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::PUstaub {

////////////////////////////////////////////////////////////////////////////////
/// Template for function (TODO)
void printPUstaubLimitsFlat 
           (const vector<fs::path>& inputs, //!< input ROOT files (histograms)
            const fs::path& outputRoot,
            const fs::path& outputTxt,
            const pt::ptree& config, //!< config handled with `Darwin::Tools::options`
            const int steering //!< parameters obtained from explicit options 
            )
{
    cout << __func__ << " start" << endl;

    auto h2 = DT::GetHist<TH2>(inputs, "clean/recptLogWgt");

    auto f = make_unique<TFile>(outputRoot.c_str(), "RECREATE");

    pt::ptree table;
    int N = h2->GetNbinsX();
    cout << "ptmin\tmaxlogw" << endl;
    for (int i=1; i<=N; ++i) {
        [[ maybe_unused ]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        int ptmin = h2->GetXaxis()->GetBinLowEdge(i);

        TH1 * p = h2->ProjectionY(Form("p%d",ptmin),i,i);
        float a = p->Integral();
        static const auto feps = numeric_limits<float>::epsilon();
        if (a < feps) continue;
        int l = p->GetNbinsX();
        while(p->GetBinContent(l) < 3)
            --l;
        float maxlogw = p->GetBinLowEdge(l);
        
        p->Scale(1./a);
        p->Write();

        // write
        cout << ptmin << ' ' << maxlogw << endl;
        table.put<float>(to_string(ptmin), maxlogw);
        
    }

    pt::write_info(outputTxt.string(), table);
    
    cout << __func__ << " stop" << endl;
}

} // end of DAS namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["CMSSW"] = getenv("CMSSW_VERSION");

        vector<fs::path> inputs;
        fs::path outputRoot, outputTxt;

        DT::Options options("Determines the maximum weight in jet high-pileup samples.\n"
                            "NB: this executable expect a flat sample!");
        options.inputs("inputs", &inputs, "input ROOT files (from `getPUstaub`)")
               .output("outputRoot", &outputRoot, "output ROOT file with histograms")
               .output("outputTxt" , &outputTxt , "output 2-column file with limits");

        const auto& config = options(argc, argv);
        const int steering = options.steering();

        DAS::PUstaub::printPUstaubLimitsFlat(inputs, outputRoot, outputTxt, config, steering);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
